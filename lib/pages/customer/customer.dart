import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:login/models/cartModel.dart';
import 'package:login/models/productModel.dart';
import 'package:login/pages/product/detail_product.dart';
import 'package:login/services/api.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class Customer extends StatefulWidget {
  final VoidCallback signOut;
  Customer(this.signOut);

  @override
  _CustomerState createState() => _CustomerState();
}

class _CustomerState extends State<Customer> {
  // Variable
  final money = NumberFormat("#,##0","en_US");
  int idUser;
  String codeBusiness;

  getPref()async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      idUser = preferences.getInt("id");
      codeBusiness = preferences.getString("code");
    });
    _showData();
  }

  var loading = false;
  final list = new List<ProductModel>(); 
  final GlobalKey<RefreshIndicatorState> _refresh = GlobalKey<RefreshIndicatorState>();

  Future<void> _showData() async{
    list.clear();
    setState(() {
      loading = true; 
    });
    // Variable
    final response = await http.get(BaseUrl.product);

    if(response.contentLength == 2){
    }else{
      final data = jsonDecode(response.body);
      final val = data['data'];
      val.forEach((api){
          final ab = new ProductModel(
            api['id'].toString(), 
            api['code'], 
            api['productname'], 
            api['qty'], 
            api['price'], 
            api['foto'], 
            api['createdby'].toString(), 
            api['createdat'],
          );
          list.add(ab);
      });
      setState(() {
        _totalCart();
        loading = false;
      });
    }
  }

  addCart(String idProduct, String price) async{
    final response = await http.post(BaseUrl.cartAdd, body: {
      "id_user" : idUser.toString(),
      "id_product" : idProduct,
      "code" : codeBusiness,
      "price" : price,
    });
    final data    = jsonDecode(response.body);
    int status    = data['status'];
    String pesan  = data['message'];

    if(status == 200){
      print(pesan);
      _totalCart();
    }else{
      print(pesan); 
    }
  }

  String total = "0";
  final ex = List<CartModel>();
  _totalCart() async{
    setState(() {
      loading = true;
    });
    ex.clear();
    final response = await http.get(BaseUrl.cartTotal+idUser.toString());
    final data = jsonDecode(response.body);
    data.forEach((api){
      final exp = new CartModel(api['total'].toString());
      ex.add(exp);
      setState(() {
        total = exp.total.toString();
      });
    });
    setState(() {
      loading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    getPref();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Button Cart
        actions: <Widget>[
          Stack(
            children: <Widget>[
              IconButton(
                onPressed: (){},
                icon: Icon(Icons.shopping_cart),
              ),
              total == "0"
              ? Container() 
              : Positioned(
                right: 0.0,
                child: Stack(
                  children: <Widget>[
                    Icon(
                      Icons.brightness_1, 
                      size: 25.0, 
                      color: Colors.yellow,
                    ),
                    Positioned(
                      top: 6.0,
                      right: 6.0,
                      child: Text(
                        total, 
                        style: TextStyle(
                          color: Colors.black, 
                          fontSize: 11.0
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),

          // Button logout
            IconButton(
              onPressed: (){
                setState(() {
                  widget.signOut();
                });
              },
              icon: Icon(Icons.lock),
            )
       ],
     ),
     body: RefreshIndicator(
      onRefresh: _showData,
      key: _refresh,
        child: loading 
        ? Center(child: CircularProgressIndicator())
        : Center(
          child: OrientationBuilder(
          builder: (context, orientation){
            return GridView.builder(
             gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
               crossAxisCount: orientation == Orientation.portrait ? 2 : 3
             ),
             itemCount: list.length,
             itemBuilder: (context, i){
               final x = list[i];
               return InkWell(
                  onTap: (){
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (context)=>DetailProduct(x)
                    ));
                  },
                  child: Card(
                   child: Column(
                     children: <Widget>[
                       Expanded(
                          child: Hero(
                            tag: x.id,
                            child: Image.network(
                              BaseUrl.productImage+x.foto, 
                              fit: BoxFit.cover,
                            ),
                          ),
                       ),
                        Text(
                          x.productname, 
                          textAlign: TextAlign.center,
                        ),
                        Text(
                          "Rp. "+money.format(int.parse(x.price)),
                          style: TextStyle(
                                    color: Colors.red
                                ),  
                        ),
                        RaisedButton(
                          onPressed: (){
                            addCart(x.id, x.price);
                          },
                          color: Colors.orange,
                          child: Text(
                            "Buy",
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        )
                     ],
                   ),
                 ),
               );
             },
           );
          },
         ),
       ),
     ),
    );
  }
}