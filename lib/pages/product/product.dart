import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:login/models/productModel.dart';
import 'package:login/pages/product/add_product.dart';
import 'package:http/http.dart' as http;
import 'package:login/pages/product/edit_product.dart';
import 'package:login/services/api.dart';
import 'package:intl/intl.dart';

class Product extends StatefulWidget {
  @override
  _ProductState createState() => _ProductState();
}

class _ProductState extends State<Product> {
  // Variable
  final money = NumberFormat("#,##0","en_US");

  var loading = false;
  final list = new List<ProductModel>(); 
  final GlobalKey<RefreshIndicatorState> _refresh = GlobalKey<RefreshIndicatorState>();

  Future<void> _showData() async{
    list.clear();
    setState(() {
      loading = true; 
    });
    // Variable
    final response = await http.get(BaseUrl.product);

    if(response.contentLength == 2){
    }else{
      final data = jsonDecode(response.body);
      final val = data['data'];
      val.forEach((api){
          final ab = new ProductModel(
            api['id'].toString(), 
            api['code'], 
            api['productname'], 
            api['qty'], 
            api['price'], 
            api['foto'], 
            api['createdby'].toString(), 
            api['createdat'],
          );
          list.add(ab);
      });
      setState(() {
        loading = false;
      });
    }
  }

  // Dialog Delete Button
  dialogDelete(String id){
    showDialog(
      context: context,
      builder: (context){
        return Dialog(
          child: ListView(
            padding: EdgeInsets.all(16.0),
            shrinkWrap: true,
            children: <Widget>[
              Text("Are you sure want to delete this product ?", style: TextStyle(
                fontSize: 18.0, 
                fontWeight: FontWeight.bold)
              ),
              SizedBox(
                height: 10.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  InkWell(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child: Text("No")
                  ),
                  SizedBox(
                    width: 16.0,
                  ),
                  InkWell(
                    onTap: (){
                      _delete(id);
                    },
                    child: Text("Yes")
                  ),
                ],
              )
            ],
          ),
        );
      }
    );
  }

  _delete(String id) async{
    final response = await http.post(BaseUrl.productDelete, body:{
      "id"  : id
    });
    final data = jsonDecode(response.body);
    int status = data['status'];
    String pesan = data['message'];
    if (status == 200) {
      setState(() {
        Navigator.pop(context);
        _showData();
      });
    } else {
      print(pesan);
    }
  }

  @override
  void initState() {
    super.initState();
    _showData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          Navigator.of(context).push(MaterialPageRoute(
            builder: (context)=>AddProduct(_showData)));
        },
        child: Icon(Icons.add),
      ),
      body: RefreshIndicator(
        onRefresh: _showData,
        key: _refresh,
        child: loading 
        ? Center(child: CircularProgressIndicator())
        : ListView.builder(
          itemCount: list.length,
          itemBuilder: (context, i) {
            final x = list[i];
            return Container(
              padding: EdgeInsets.all(10.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Image.network(BaseUrl.productImage+x.foto, 
                    width: 100.0,
                    height: 180.0,
                    fit: BoxFit.cover,
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(x.productname, 
                          style: TextStyle(
                            fontSize: 18.0, 
                            fontWeight: FontWeight.bold
                          ),
                        ),
                        Text(x.qty),
                        Text(money.format(int.parse(x.price))),
                        Text(x.createdat),
                      ],
                    ),
                  ),
                  IconButton(
                    onPressed: (){
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context)=>EditProduct(x, _showData)
                      ));
                    },
                    icon: Icon(Icons.edit),
                  ),
                  IconButton(
                    onPressed: (){
                      dialogDelete(x.id);
                    },
                    icon: Icon(Icons.delete),
                  ),
                ],
              ),
            );
          },
        ),
      ), 
    );
  }
}