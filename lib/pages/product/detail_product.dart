import 'package:flutter/material.dart';
import 'package:login/models/productModel.dart';
import 'package:login/services/api.dart';

class DetailProduct extends StatefulWidget {
  final ProductModel model;
  DetailProduct(this.model);

  @override
  _DetailProductState createState() => _DetailProductState();
}

class _DetailProductState extends State<DetailProduct> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled){
          return <Widget>[
            SliverAppBar(
              expandedHeight: 200.0,
              floating: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                background: Hero(
                  tag: widget.model.id,
                  child: Image.network(
                    BaseUrl.productImage+widget.model.foto, 
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            )
          ];
        },
        body: Container(
          child: Stack(
            children: <Widget>[
              Positioned(
                top: 30.0,
                right: 10.0,
                left: 10.0,
                child: Column(
                  children: <Widget>[
                    Text(widget.model.productname),
                    Text(widget.model.price),
                  ],
                ),
              ),
              Positioned(
                bottom: 10.0,
                left: 0.0,
                right: 0.0,
                child: Container(
                  padding: EdgeInsets.all(16.0),
                  child: Material(
                    color: Colors.orange,
                    borderRadius: BorderRadius.circular(10.0),
                    child: MaterialButton(
                      onPressed: (){},
                      child: Text(
                        "Add to Cart",
                        style: TextStyle(
                          color: Colors.white
                        )
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}